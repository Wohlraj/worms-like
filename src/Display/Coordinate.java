/**
 * @author GRANGER Hugues
 */
package Display;
public class Coordinate {

	private int x;
	private int y;
	
	private Coordinate next;

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	public void setVal(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Coordinate getNext() {
		return this.next;
	}

	public void setNext(Coordinate next) {
		this.next = next;
	}

	public String toString() {
		return x+","+y; 
	}
	
	public Coordinate(float x2, float y2) {
		this.next = null;
		this.x = (int) x2;
		this.y = (int) y2;
	}
}
