/**
 * @author GRANGER Hugues
 */
package Display;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class DispMap {
	private Map map;

	public DispMap(Map map) {
		this.map = map;
	}
	
	private static BufferedImage image[] = new BufferedImage[1];
	static {
		try {
			image[0] = ImageIO.read(new File("sky.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display(Graphics g) {
		Graphics2D gr = (Graphics2D)g;
		gr.drawImage(image[0], null, 0, 0);
		gr.drawImage(image[0], null, 300, 0);
		gr.drawImage(image[0], null, 0, 300);
		gr.drawImage(image[0], null, 300, 300);
		gr.drawImage(image[0], null, 600, 0);
		gr.drawImage(image[0], null, 600, 300);
		gr.drawImage(image[0], null, 900, 0);
		gr.drawImage(image[0], null, 900, 300);
		
		g.setColor(Color.yellow);
		Functions f=new Functions();

		Coordinate res = null;
		Polygon pol = map.getDecor().getFirst(); 
		while(pol != null && res==null) {
			int x[], y[];
			x = f.polygonToLists(pol)[0];
			y = f.polygonToLists(pol)[1];
			int n = Functions.countSommetsPolygon(pol);
			for(int l=0;l<n;l++)
				y[l]=map.getHeight()-y[l];
			g.fillPolygon(x, y, n);

			pol=pol.getNext();
		}

		for(int i=1; i<100; i=i+5) {
			for(int j=200; j<100; j=j+5){
				if(f.isInPolygon(new Coordinate(i,j), map.getDecor().getFirst()))
					g.drawOval(i,map.getHeight()-j,5,5);
			}
		}
		
		g.setColor(Color.black);
		g.drawString("Touches Q et D pour se d�placer", 10, map.getHeight()-35);
	}
}