/**
 * @author GRANGER Hugues
 */
package Display;

import java.awt.Color;
import java.awt.Graphics;

public class DispPolygon {
	private Polygon polygon;
	
	public DispPolygon(Polygon polygon) {
		this.polygon = polygon;
	}
	
	public void display(Graphics g, int map_height) {
		g.setColor(Color.red);
		Coordinate tmp0 = polygon.getFirst();
		Coordinate tmp1 = tmp0;
		int i=0;
		while(tmp1 != tmp0 && tmp1 != null || i==0) {
			g.drawRect(tmp1.getX(), map_height - tmp1.getY(), 3, 3);
			g.drawLine(tmp1.getX(), map_height - tmp1.getY(), tmp1.getNext().getX(), map_height - tmp1.getNext().getY());
			tmp1 = tmp1.getNext();
			i++;
		}	
	}
}
