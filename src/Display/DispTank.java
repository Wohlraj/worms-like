/**
 * @author GRANGER Hugues
 */
package Display;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class DispTank {
	private Tank tank;
	
	public DispTank(Tank tank) {
		this.tank = tank;
	}
	
	private static BufferedImage image[] = new BufferedImage[1];
	static {
		try {
			image[0] = ImageIO.read(new File("tank.gif"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display(Graphics g, int map_height, int map_width) {
		/* affichage du tank */		
		Graphics2D gr = (Graphics2D)g;
		gr.drawImage(image[0], null, -20+tank.getX(), map_height-tank.getY()-15);
		
		/* affichage du canon */
		int canonX = (int) (tank.getX()+Math.floor(25*Math.cos(tank.getAlpha())));
		int canonY = (int) (map_height-tank.getY()-15-Math.floor(25*Math.sin(tank.getAlpha())));
		g.drawLine(tank.getX(),map_height-tank.getY()-15, canonX, canonY);
		
		/* affichage de la barre de vie */
		g.setColor(Color.white);
		g.fillRect(tank.getX()-20, map_height-tank.getY()+15, 41, 5);
		g.setColor(Color.red);
		g.fillRect(tank.getX()-20, map_height-tank.getY()+15, (int) Math.max(0,Math.floor(41*tank.getHealth()/100)), 5);
		
		/* affichage du pseudo */
		g.setColor(Color.black);
		g.drawString(tank.getName(), tank.getX()-20, map_height-tank.getY()+30);
		
		/* affichage du gagnant */
		if(tank.getHealth()<=0)
			g.drawString(tank.getName()+" a perdu !", map_width/2-30, 30);
	}
}