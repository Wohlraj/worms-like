/**
 * @author COURDURIER Alain
 */
package Display;

import java.lang.Math;

public class Euler {
	private double g = 1; // constante de gravitation.
	private double C = 0.007; // coefficient de frottement
	private double h = 0.05; // pas d'int�gration

	/**
	 * calcule la norme
	 * 
	 * @return la norme
	 */
	private double normSpeed(float Vx, float Vy) {
		return Math.sqrt(Vx*Vx + Vy*Vy);
	}

	public float[] go(float x,float y,float Vx,float Vy){
		try {
			float Vx_tmp= Vx; /* permet de conserver l'ancienne vitesse avant d'�tre �cras�e par la nouvelle */
			float Vy_tmp = Vy;
			Vx = (float) (-C*normSpeed(Vx_tmp,Vy_tmp)*Vx_tmp*h + Vx_tmp);
			Vy = (float) (-g*h-C*h*normSpeed(Vx_tmp,Vy_tmp)*Vy_tmp + Vy_tmp);
			x = (float) (x + h * Vx); // x(t+h)=x(t)+hVx
			y = (float) (y + h * Vy); //y(t+h)=y(t)+hVy
		}
		catch (Exception e) { // bloc d'instructions ex�cut� en cas d'exception.
			System.out.println("Fin d'execution");
		}
		return new float[]{x,y,Vx,Vy};
	}
}

