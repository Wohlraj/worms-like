/**
 * @author GRANGER Hugues
 */
package Display;

import java.util.LinkedList;

public class Functions {
	
	private float x;
	private float y;
	
	public Functions(){
		
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	

	/**
	 * @return l'origine du segment intersect� s'il existe, null sinon
	 */
	public Coordinate isSecantWithPolygon(Coordinate P, Polygon Pol) {
		Boolean res = false;
		
		Coordinate A = Pol.getFirst(); 
		/* A est la premiere coordonnee du polygone */
		res = (isSecantWithSegment(P, A)!=null);
		
		Coordinate A_tmp = A.getNext();
		Coordinate A_tmp2 = A_tmp;
		int i=0; /* permet de boucler la boucle */
		while(A_tmp != A.getNext() && !res || i==0) {
			/* System.out.println("a_tmp : "+A_tmp.toString()+"->"+A_tmp.getNext().toString()+" P : "+P.toString()); */
			res = isSecantWithSegment(P, A_tmp)!=null;
			A_tmp2 = A_tmp;
			A_tmp = A_tmp.getNext();
			i++;
		} 
		if(res)
			return A_tmp2;
		return null;
	}
	
	/**
	 * Determine l'intersection de deux segments
	 * @param P coordonnees du projectile
	 * @param O coordonnees de l'objet
	 * @return [x,y] coordonn�es de l'intersection
	 */
	public int[] isSecantWithSegment(Coordinate P, Coordinate O) {
		
		int res[] = null;
		
		/* coordonees du segment-objet */
		int O1_x = O.getX();
		int O1_y = O.getY();
		int O2_x = O.getNext().getX();
		int O2_y = O.getNext().getY();
		
		float[] projEquation = lineEquation(P.getX(), P.getY(), P.getNext().getX(), P.getNext().getY());
		float[] objectEquation = lineEquation(O1_x, O1_y, O2_x, O2_y);
	
		
		float a0 = objectEquation[0];
		float b0 = objectEquation[1];
		float c0 = objectEquation[2];
		float a1 = projEquation[0];
		float b1 = projEquation[1];
		float c1 = projEquation[2];

		/* est-ce que la trajectoire traverse le segment ? */
		if ((a0*P.getNext().getX()+b0*P.getNext().getY()+c0)*(a0*P.getX()+b0*P.getY()+c0)<=0)
			/* si on a �galite ci-dessus, c'est que une coordonn�es du polygone est sur l'objet */
			{
				res = new int[2];
				float det = a0*b1-a1*b0;
				if (det==0) {
					/* this.x = O1_x;
					this.y = O1_y; */
					/* System.out.println("Attention : droites parrall�les"); */
				}
				else {
					res[0] = Math.round((-c0*b1+b0*c1)/det);
					res[1] = Math.round((c0*a1-c1*a0)/det);
				}	
				/* on verifie que l'impact appartient bien au segment et pas seulement e la droite */
				if  (
						((O1_x<=res[0] && res[0]<=O2_x) || (O1_x>=res[0] && res[0]>=O2_x))
						&&
						((O1_y<=res[1] && res[1]<=O2_y) || (O1_y>=res[1] && res[1]>=O2_y))
					)
					return res;
			}
		/*
		if(res==null)
			System.out.println(O1_x+","+O1_y+" vers "+O2_x+","+O2_y+" n'intersecte pas "+P.getX()+","+P.getY()+" vers "+P.getNext().getX()+","+P.getNext().getY());
		else
			System.out.println(O1_x+","+O1_y+" vers "+O2_x+","+O2_y+" intersecte !! "+P.getX()+","+P.getY()+" vers "+P.getNext().getX()+","+P.getNext().getY());
			*/
		return null;
	}
	
	/**
	 * Calcul d'une equation de droite � partir de deux points
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return alpha, beta et gamma, coefficients definissant la droite
	 */
	public float[] lineEquation(int x1,int y1,int x2,int y2) {
		float[] tab = new float[3];	
		if(x1 == x2) {
			tab[0] = 1;
			tab[1] = 0;
			tab[2] = -x1;
		}
		else {
			tab[0] = (float) -(y2-y1)/(x2-x1);
			tab[1] = 1;
			tab[2] = -(y1+tab[0]*x1);
		}
		return tab;
	}
	
	/**
	 * Permet de savoir si une coordonee est un sommet d'un polygone ou non
	 * @param c coordonee e chercher
	 * @param p polygone oe l'on cherche
	 * @return la coordonee que l'on cherchait (celle du polygone p !), si elle existe. Sinon, retourne null.
	 */
	public Coordinate isOnPolygon(Coordinate c, Polygon Pol) {
		Coordinate A = Pol.getFirst(); 
		Coordinate A_tmp = A;
		int i = 0; /* permet de boucler la boucle entierement */
		while(( A_tmp != A || i==0 ) && (A_tmp.getX() != c.getX() || A_tmp.getY() != c.getY())) {
			A_tmp = A_tmp.getNext();
			i++;
		}
		
		if(A_tmp.getX() == c.getX() && A_tmp.getY() == c.getY())
			return A_tmp;
		else
			return null;				
	}
	
	/**
	 * Permet de savoir si une coordonee est � l'int�rieur d'un polygone ou non
	 * @param c coordonee e chercher
	 * @param p polygone oe l'on cherche
	 * @return true ou false
	 */
	public Boolean isInPolygon(Coordinate c, Polygon Pol){
		Coordinate a = new Coordinate(c.getX(), c.getY());
		Coordinate b = new Coordinate(c.getX()+1, c.getY());
		a.setNext(b);
		int count = 0;
		/* TODO 1000 valeur arbitraire d�pendant de la taille de la carte... */
		while(a.getX()<1000) {			
			if(isSecantWithPolygon(a, Pol)!=null)
				count++;
			a.setVal(a.getX()+1, a.getY());
			b.setVal(a.getX()+2, a.getY());
		}
		return (count%2)==1;
	}
	
	/**
	 * Permet de savoir si un polygone est � l'int�rieur d'un autre polygone
	 * @param P polygon int�rieur
	 * @param Pol polygone ext�rieur
	 * @return null ou bien une coordonn�e qui n'est pas dans le polygone Pol
	 */
	public Coordinate isInPolygon(Polygon P, Polygon Pol){
		Coordinate A = P.getFirst(); 
		Coordinate A_tmp = A;
		int i=0;
		while(A_tmp != A || i==0) {
			/* si A_tmp est dans le polygone, il faut passer au suivant */
			if(isInPolygon(A_tmp,Pol))
				A_tmp=A_tmp.getNext();
			else
				break;
			i++;
		}
		if(A_tmp==A && i!=0)
			return null;
		return A_tmp;
	}

	
	
	/**
	 * Determine les points d'intersection entre deux polygones
	 * @return la liste des points ET modifie les deux polygones en y ajoutant les intersections 
	 */
	public LinkedList<Coordinate> interPolygons(Polygon P1, Polygon P2) {
		LinkedList<Coordinate> list = new LinkedList<Coordinate>();
		Coordinate A1 = P1.getFirst(); 
		Coordinate A1_tmp = A1;
		int i1 = 0; /* permet de boucler la boucle entierement */
		while(A1_tmp != A1 || i1==0) {
			
			Coordinate A2 = P2.getFirst(); 
			Coordinate A2_tmp = A2;
			int i2 = 0; /* permet de boucler la boucle entierement */
			while(A2_tmp != A2 || i2==0) {
				/* ici, on a un segment de chaque polygone : il faut trouver une intersection eventuelle */
				int[] inter = isSecantWithSegment(A1_tmp, A2_tmp);
				if(inter != null) {
					Coordinate I1 = new Coordinate(inter[0], inter[1]);
					/* on evite les boucles infinies si un coin est aussi une intersection */
					/* on ajoute ici l'intersection avec le polygone 1 */
					if(isOnPolygon(I1,P1)==null && (inter[0] != A1_tmp.getNext().getX() ||  inter[1] != A1_tmp.getNext().getY())) {
						I1.setNext(A1_tmp.getNext());
						A1_tmp.setNext(I1);
						A1_tmp=A1_tmp.getNext();
					}
					/* on ajoute ici l'intersection avec le polygone 2 */
					Coordinate I2 = new Coordinate(inter[0], inter[1]);
					if(isOnPolygon(I2,P2)==null && (inter[0] != A2_tmp.getNext().getX() ||  inter[1] != A2_tmp.getNext().getY())) {
						I2.setNext(A2_tmp.getNext());
						A2_tmp.setNext(I2);
						A2_tmp = A2_tmp.getNext();
					}
					/* System.out.println("Intersection en "+x+", "+y+" / "+A1_tmp.toString()+"->"+A1_tmp.getNext().toString()+";"+A2_tmp.toString()+"->"+A2_tmp.getNext().toString()); */									
				}
				
				i2++;
				A2_tmp = A2_tmp.getNext();
			} 
			
			i1++;
			A1_tmp = A1_tmp.getNext();
		}
		return list;
	}
	
	/**
	 * permet de savoir si l'on doit arr�ter un mouvement � cause d'un obstacle
	 * @param map
	 * @param origin
	 * @return l'origine du segment intersect� s'il existe, null sinon
	 */
	public Coordinate moveStops(Map map, Coordinate origin) {
		/* renvoie true si la trajectoire rencontre un polygone quelconque */
		/* on parcourt le decor entier : */
		Coordinate res = null;
		Polygon Pol = map.getDecor().getFirst(); 
		while(Pol != null && res==null) {
			res = isSecantWithPolygon(origin, Pol);
			Pol = Pol.getNext();
		}
		return res;
	}
	
	public static int countSommetsPolygon(Polygon pol){
		int k=0;
		Coordinate A = pol.getFirst();
		Coordinate A_tmp = A;
		while(A_tmp != A || k==0) {
			A_tmp = A_tmp.getNext();
			k++;
		} 
		return k;
	}
	
	public int[][] polygonToLists(Polygon pol){
		int n = countSommetsPolygon(pol);
		int x[] = new int[n];
		int y[] = new int[n];
		Coordinate A = pol.getFirst();
		Coordinate A_tmp = A;
		int k=0; /* permet de boucler la boucle */
		while(A_tmp != A || k<n-1) {
			x[k]=A_tmp.getX();
			y[k]=A_tmp.getY();
			A_tmp = A_tmp.getNext();
			k++;
		} 
		int[][] res = new int[n][2];
		res[0] = x;
		res[1] = y;
		return res;
	}
	
	public static void main(String[] args){
		Coordinate start = new Coordinate(600, 450);
		Polygon fig = new Polygon(start); 
		fig.insertFirst(new Coordinate(800, 450));
		fig.insertFirst(new Coordinate(800, 550));
		start.setNext(fig.getFirst()); 
		
		System.out.println(fig.toString());
		System.out.println(countSommetsPolygon(fig));
	}
}
