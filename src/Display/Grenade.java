/**
 * @author COURDURIER Alain
 */
package Display;

public class Grenade extends Weapon {

	public Grenade() {
		super("Grenade", 50, 50, 15);
	}
	
}