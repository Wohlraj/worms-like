/**
 * @author GRANGER Hugues
 */
package Display;
import java.util.Random;

public class Map {
	
	private int width;	
	private int height;	
	private PolygonList decor;	
	private DispMap disp;
	
	public DispMap getDisp(){
		return this.disp;
	}
	
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;

	}
			
	public Map(String name, int width, int height, PolygonList decor) {
		this.disp = new DispMap(this);
		this.width = width;
		this.height = height;
		this.decor = decor;
		
		buildDecor();
	}
	
	private void buildDecor() {
		Coordinate start = new Coordinate(0, 0);
		Polygon fig1 = new Polygon(start); 
		fig1.insertFirst(new Coordinate(1000, 0));
		for(int i=0; i<=10; i++){
			Random r = new Random(); 
			int y = 200+r.nextInt(200);
			fig1.insertFirst(new Coordinate(1000-100*i, y));
		}
		start.setNext(fig1.getFirst()); 
		decor.insertFirst(fig1);

		Coordinate start2 = new Coordinate(0, 400);
		Polygon fig2 = new Polygon(start2); 
		fig2.insertFirst(new Coordinate(100, 400));
		fig2.insertFirst(new Coordinate(100, 500));
		fig2.insertFirst(new Coordinate(0, 550));
		start2.setNext(fig2.getFirst()); 
		decor.insertFirst(fig2);
		
		Coordinate start3 = new Coordinate(600, 450);
		Polygon fig3 = new Polygon(start3); 
		fig3.insertFirst(new Coordinate(800, 450));
		fig3.insertFirst(new Coordinate(800, 550));
		start3.setNext(fig3.getFirst()); 
		decor.insertFirst(fig3);
	}
	public void setDecor(PolygonList decor) {
		this.decor = decor;
	}
	public PolygonList getDecor() {
		return decor;
	}
}
