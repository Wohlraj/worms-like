/**
 * @author COURDURIER Alain
 */
package Display;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class NewJFrame extends javax.swing.JFrame {

	private JPanel panelNord;
	private JPanel panelCentre;
	private JPanel panelSud;

	private JButton boutonJouer;
	private JList boutonChoix;
	private JButton boutonStop;

	private static Simulation simu = null;

	Graphics bufferGraphics; 
	Image offscreen; 

	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				NewJFrame inst = new NewJFrame();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public NewJFrame() {
		super();
		initGUI();
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			{
				panelNord = new JPanel();
				FlowLayout panelNordLayout = new FlowLayout();
				getContentPane().add(panelNord, BorderLayout.NORTH);
				panelNord.setLayout(panelNordLayout);
				panelNord.setPreferredSize(new java.awt.Dimension(1000, 50));
				{
					boutonJouer = new JButton();
					panelNord.add(boutonJouer);
					boutonJouer.setText("Jouer !");
					boutonJouer.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							boutonJouerActionPerformed(evt);
						}
					});
				}
				{
					boutonStop = new JButton();
					panelNord.add(boutonStop);
					boutonStop.setText("Quitter");
					boutonStop.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							System.exit(0) ; 
						}
					});
				}
				{
					ListModel boutonChoixModel = 
						new DefaultComboBoxModel(
								new String[] { "Bazooka", "Grenade" });
					boutonChoix = new JList();
					panelNord.add(getBoutonChoix());
					boutonChoix.setModel(boutonChoixModel);
				}
			}
			{
				panelCentre = new JPanel();
				getContentPane().add(panelCentre, BorderLayout.CENTER);
				panelCentre.setFocusable(true);
				panelCentre.requestFocus();
				panelCentre.setPreferredSize(new java.awt.Dimension(1000, 600));
				panelCentre.addMouseMotionListener(new MouseMotionAdapter() {
					public void mouseMoved(MouseEvent evt) {
						panelCentreMouseMoved(evt);
					}
				});
				panelCentre.addKeyListener(new KeyAdapter() {
					public void keyTyped(KeyEvent evt) {
						panelCentreKeyTyped(evt);
					}
				});
				panelCentre.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent evt) {
						panelCentreMouseClicked(evt);
					}
				});
			}
			{
				JPanel panelCentre0 = new JPanel();
				getContentPane().add(panelCentre0, BorderLayout.CENTER);
			}
			{
				panelSud = new JPanel();
				getContentPane().add(panelSud, BorderLayout.SOUTH);
				FlowLayout panelSudLayout = new FlowLayout();
				panelSud.setVisible(false);
				panelSud.setPreferredSize(new java.awt.Dimension(1000, 0));
			}
			pack();
			this.setSize(1010, 650);
		} catch (Exception e) {
			e.printStackTrace();
		}

		offscreen = createImage(1000, 600); 
		bufferGraphics = offscreen.getGraphics(); 
	}

	public void update(Graphics g) 
	{ 
		super.paint(g); 
	} 

	@Override
	public void paint(Graphics g) {
		if(simu != null)
			simu.display(bufferGraphics);	
		g.drawImage(offscreen, 0, 75, panelCentre);
	}

	private void panelCentreMouseClicked(MouseEvent evt) {
		if(boutonChoix.getSelectedIndex()==0)
			simu.getTank().setWeapon(new Bazooka());
		if(boutonChoix.getSelectedIndex()==1)
			simu.getTank().setWeapon(new Grenade());

		simu.playerFires();
		simu.nextPlayer();
		repaint();	
	}

	private void panelCentreKeyTyped(KeyEvent evt) {
		Tank t = simu.getTank();
		if(evt.getKeyChar()=='d'){
			t.move(2);
			repaint();
		}
		else if(evt.getKeyChar()=='q'){
			t.move(-2);
			repaint();
		}
	}

	private void boutonJouerActionPerformed(ActionEvent evt) {
		simu = new Simulation();
		simu.setTankNames("Joueur 1", "Joueur 2");
		simu.nextPlayer();

		this.add(panelCentre);
		panelCentre.setVisible(true);
		panelSud.setVisible(true);
		panelCentre.requestFocus();
		repaint();
	}

	private void panelCentreMouseMoved(MouseEvent evt) {
		panelCentre.requestFocus();

		double x = evt.getPoint().getX()-simu.getTank().getX();
		double y = simu.getMap().getHeight()-evt.getPoint().getY()-simu.getTank().getY();

		float alpha;
		if(x>0)
			alpha = (float) Math.atan(y/x);
		else if(x<0)
			alpha = (float) (Math.PI+Math.atan(y/x));
		else
			alpha = (float) ((Math.PI/2)*Math.abs(y)/y);

		simu.getTank().setAlpha(alpha);
		repaint();
	}

	public JList getBoutonChoix() {
		return boutonChoix;
	}
}