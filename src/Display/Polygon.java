/**
 * @author GRANGER Hugues
 */
package Display;
public class Polygon {

	private Coordinate first;
	protected DispPolygon disp;
	/**
	 * lien pour la sous-liste
	 */
	private Polygon next;	

	public Polygon getNext() {
		return this.next;
	}

	public void setNext(Polygon next) {
		this.next = next;
	}

	public String toString() {
		Coordinate A = getFirst(); 		
		Coordinate A_tmp = A.getNext();
		String res = A.getX()+","+A.getY();
		while(A_tmp != A && A_tmp != null) {
			res=res+"||"+A_tmp.getX()+","+A_tmp.getY();
			A_tmp = A_tmp.getNext();
		}
		return res;
	}
	
	public Polygon(Coordinate first) {
		this.next = null;
		this.first = first;
		this.disp = new DispPolygon(this);
	}

	public void insertFirst(Coordinate c) {
		c.setNext(this.first);
		this.first = c;
	}

	public Coordinate getFirst() {
		return first;
	}

	public void setFirst(Coordinate c) {
		this.first = c;
	}
	
	/**
	 * permet de boucler un polygone tout en inversant l'ordre de la liste,
	 * sert pour la reconstruction apr�s explosion
	 */
	public void reverseAndCycle() {
		Coordinate A = getFirst(); 
		Polygon res = new Polygon(A);		
		Coordinate A_tmp = A.getNext();
		while(A_tmp != null) {
			res.insertFirst(new Coordinate(A_tmp.getX(), A_tmp.getY()));
			A_tmp=A_tmp.getNext();
		}
		
		A.setNext(res.getFirst()); /* bouclage */
	}
	
	/**
	 * permet d'inverser l'ordre de la liste,
	 * sert pour la reconstruction apr�s explosion
	 * on coupe la chaine pour pouvoir la reconstruire � l'envers
	 */
	public void reverse() {
		Coordinate tmp = getFirst();
		setFirst(tmp.getNext());
		tmp.setNext(null);
		reverseAndCycle();
	}
	
	public DispPolygon getDisp() {
		return this.disp;
	}	
	
	public static void main(String[] args){
		Coordinate start = new Coordinate(4, 4);
		Polygon fig = new Polygon(start); 
		fig.insertFirst(new Coordinate(3, 3));
		fig.insertFirst(new Coordinate(2, 2));
		fig.insertFirst(new Coordinate(1, 1));
		start.setNext(fig.getFirst()); 
		
		System.out.println("1) "+fig.toString());
		fig.reverse();
		System.out.println("2) "+fig.toString());
	}
}
