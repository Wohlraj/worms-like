/**
 * @author GRANGER Hugues
 */
package Display;
public class PolygonList {

	private Polygon first;

	public PolygonList() {
		first = null;
	}

	public Polygon getFirst() {
		return first;
	}

	public void setFirst(Polygon first) {
		this.first = first;
	}
	
	public void insertFirst(Polygon val) {
		val.setNext(first);
		first = val;
	}

	public void insertLast(Polygon val) {
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Polygon elem = first; elem != null; elem = elem.getNext()) {
			sb.append(elem.toString() + " | ");
		}
		return sb.toString();
	}

}
