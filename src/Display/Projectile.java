/**
 * @author GRANGER Hugues
 */
package Display;
import java.lang.Math;

public class Projectile {

	private Functions f; /* contient la position du projectile */

	private float Vx;
	private float Vy;

	private Map map;

	private Weapon ammo;

	private Polygon trajectory;


	public Polygon getTrajectory() {
		return trajectory;
	}

	/**
	 * Constructeur du projectile
	 * @param x position de depart horizontale
	 * @param y verticale
	 * @param map carte sur laquelle il est tire
	 */
	public Projectile(float x, float y, float alpha, Map map, Weapon ammo) {
		this.f = new Functions();		
		f.setX(x);
		f.setY(y);

		this.Vx = (float) (ammo.getSpeed()*Math.cos(alpha));
		this.Vy = (float) (ammo.getSpeed()*Math.sin(alpha));

		this.ammo = ammo;
		this.map = map;
		this.trajectory = new Polygon(new Coordinate(x, y));
		trajectory();		
	}

	/**
	 * Calcul de la trajectoire apres lancement
	 */
	public void trajectory() {
		moveForward();
		while( f.moveStops( map, trajectory.getFirst() )==null && f.getX()<map.getWidth() && f.getX()>0 && f.getY()>0) {
			moveForward();			
		}
		//System.out.println("Affichage de la trajectoire : "+trajectory.toString());
		explode();
	}

	/**
	 * Etape suivante dans l'algorithme d'Euler
	 */
	private void moveForward() {		
		float[] moveTab = new Euler().go(f.getX(), f.getY(), Vx, Vy);
		f.setX(moveTab[0]);
		f.setY(moveTab[1]);
		this.Vx = moveTab[2];
		this.Vy = moveTab[3];
		trajectory.insertFirst(new Coordinate(f.getX(), f.getY()));
	} 	

	/**
	 * Explosion du projectile et destruction du decor
	 */
	public void explode() {
		//System.out.println("Explosion en "+f.getX()+", "+f.getY());

		/* on v�rifie que l'on est pas sur un bord, si c'est le cas on ne d�truit pas les polygones */
		if(f.getX() != 0 && f.getX() != map.getWidth() && f.getY() != 0 && f.getY() != map.getHeight()) {			

			Polygon Pol = map.getDecor().getFirst(); 
			while(Pol != null) { /* on parcourt tous les polygones du decor */

				int damageArea = ammo.getDamageArea();
				/* on cree le polygone d'explosion */
				Polygon explosion = new Polygon(null);
				Coordinate start = new Coordinate(f.getX()+damageArea, f.getY());
				explosion.insertFirst(start);
				for(int i=1; i<10; i++){
					explosion.insertFirst(
							new Coordinate(
									(int) (f.getX()+Math.cos(2*Math.PI*i/10)*damageArea),
									(int) (f.getY()+Math.sin(2*Math.PI*i/10)*damageArea))
					);
				}

				start.setNext(explosion.getFirst()); 

				f.interPolygons(explosion, Pol);
				explosion.reverse(); /* on retablit l'ordre */

				/* reconstruction du polygone touche */
				Coordinate A = f.isInPolygon(Pol, explosion);
				Coordinate A_tmp = A;

				if(A==null){
					System.out.println("Cas particulier : polygone dans l'explosion");
					Pol = Pol.getNext();
				}
				else {					
					Polygon Pol_res = new Polygon(null);
					int i = 0; /* permet de boucler la boucle entierement */
					int retour_bool = 0; 
					Coordinate deviation = null;
					/* permet de determiner si on vient de sortir d'une deviation ou non.
					necessaire lorsque l'intersection est egalement le coin d'un polygone, empeche
					de reprendre directement la deviation et donc une boucle infinie !
					retour_bool = 1 <=> on vient de sortir d'une deviation */
					while(A_tmp != A || i==0) {
						if(retour_bool == 1) 
							deviation = null;
						else
							deviation = f.isOnPolygon(A_tmp, explosion);

						if(deviation != null) {/* on est sur la deviation */
							Coordinate retour = f.isOnPolygon(deviation.getNext(), Pol);
							if(retour != null) {
								retour_bool = 1;
								A_tmp=retour;
								Pol_res.insertFirst(new Coordinate(retour.getX(), retour.getY()));
							}
							else {
								retour_bool = 0;
								A_tmp=deviation.getNext();
								Pol_res.insertFirst(new Coordinate(deviation.getNext().getX(), deviation.getNext().getY()));
							}

						}

						if(deviation == null) {
							A_tmp = A_tmp.getNext();
							Pol_res.insertFirst(new Coordinate(A_tmp.getX(), A_tmp.getY()));
						}

						i++;

					} 

					/* polygone suivant */
					Pol_res.reverseAndCycle();
					Pol.setFirst(Pol_res.getFirst());
					Pol = Pol.getNext();
				}
			}
		}
	}
}