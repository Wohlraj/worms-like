/**
 * @author GRANGER Hugues
 */
package Display;
import java.awt.Color;
import java.awt.Graphics;
/**
 * Jeu de type "Worms"
 * 
 * @authors H. Granger, A. Courdurier
 * 
 */
public class Simulation {

	private Map map;
	private Tank t1;
	private Tank t2;

	private Tank activeTank;
	private Projectile lastProjectile;

	/**
	 * @param args
	 */
	public Simulation(){
		this.lastProjectile = null;
		/*
		 * g�n�ration du d�cor
		 */
		PolygonList decor = new PolygonList();

		/*
		 * g�n�ration de la carte
		 */
		map = new Map("map_test", 1000, 600, decor);

		/*
		 * g�n�ration des joueurs
		 */
		this.t1 = new Tank(map);
		this.t2 = new Tank(map);
		t1.tankFalls();
		t2.tankFalls();
	}

	public Tank getTank(){
		return activeTank;
	}

	public void playerFires() {
		/* on "cr�e" (donc on tire) un projectile */
		Weapon weapon = activeTank.getWeapon();
		this.lastProjectile = new Projectile(activeTank.getX(), activeTank.getY() + 5, activeTank.getAlpha(), map, weapon);
		Coordinate impactPoint = lastProjectile.getTrajectory().getFirst();
		int i_x = impactPoint.getX();
		int i_y = impactPoint.getY();
		double d1 = Math.sqrt(Math.pow((t1.getX()-i_x),2)+Math.pow((t1.getY()-i_y),2));
		double d2 = Math.sqrt(Math.pow((t2.getX()-i_x),2)+Math.pow((t2.getY()-i_y),2));
		t1.setHealth((int) (t1.getHealth()-Math.max(0, weapon.getDamage()*(weapon.getDamageArea()-d1)/weapon.getDamageArea())));
		t2.setHealth((int) (t2.getHealth()-Math.max(0, weapon.getDamage()*(weapon.getDamageArea()-d2)/weapon.getDamageArea())));
	}

	/**
	 * Syst�me de tour par tour
	 */
	public void nextPlayer() {
		t1.tankFalls();
		t2.tankFalls();

		if(t1.getHealth() > 0 && t2.getHealth() > 0) {
			if(activeTank == null)
				activeTank = t1;
			else if(activeTank == t1)
				activeTank = t2;
			else
				activeTank = t1;
		}
	}

	/**
	 * affichage graphique 2D
	 */
	public void display(Graphics g) {
		/* d�coration visuelle */
		map.getDisp().display(g);

		/* impression de la trajectoire */
		g.setColor(Color.black);
		if(lastProjectile != null) {
			Coordinate tmp = lastProjectile.getTrajectory().getFirst();
			g.drawOval(tmp.getX(), map.getHeight() - tmp.getY(), 10, 10);
			while(tmp != null) {
				g.drawOval(5+tmp.getX(), 5+map.getHeight() - tmp.getY(), 1, 1);
				tmp = tmp.getNext();
			}				
		}


		/* impression des tanks */
		t1.getDisp().display(g, map.getHeight(), map.getWidth());
		t2.getDisp().display(g, map.getHeight(), map.getWidth());

		/* impression du squelette du d�cor */
		
		Polygon decor_part = map.getDecor().getFirst();	
		while(decor_part != null) {
			decor_part.getDisp().display(g, map.getHeight());
			decor_part = decor_part.getNext();
		}
		 

	}

	public Map getMap() {
		return map;
	}

	public void setTankNames(String namePlayer1, String namePlayer2) {
		t1.setName(namePlayer1);
		t2.setName(namePlayer2);
	}
}