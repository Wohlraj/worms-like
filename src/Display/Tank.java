/**
 * @author COURDURIER Alain
 */
package Display;
import java.util.Random;

public class Tank {
	protected DispTank disp;
	
	private Weapon weapon;
	private Map map;
	/*
	 * position horizontale du tank
	 */
	private int x;
	/*
	 * position verticale du tank
	 */
	private int y;
	/*
	 * sant� du tank
	 */
	private String name;
	/*
	 * nom du joueur
	 */	
	private int health;
	
	private float alpha;
	
	public String getName() {
		return name;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setHealth(int health){
		this.health = health;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public void setAlpha(float alpha) {
		this.alpha = alpha;		
	}
	
	public float getAlpha() {
		return alpha;
	}
	
	public int getHealth() {
		return health;
	}
	
	public Tank(Map map) {
		health = 100;
		this.name = "";
		this.map = map;
		this.disp = new DispTank(this);
		this.setWeapon(new Bazooka());
		Random r = new Random(); 
		x = r.nextInt(map.getWidth());
		y = map.getHeight();
		alpha = (float) 1;
	}
	
	public String toString() {
		return "Position de "+name+" : "+x+" "+y+", sa vie est de "+health+"/100.";
	}
	public DispTank getDisp() {
		return this.disp;
	}

	public void move(int i) {
		Functions f = new Functions();
		float slope = 0;
		float slope_max = 2;
		
		Coordinate tankMovement = new Coordinate(x, y);
		tankMovement.setNext(new Coordinate(x+i,y));
		Coordinate obstacle = f.moveStops(map, tankMovement);
		
		if(obstacle!=null) {
			float[] equation_slope = f.lineEquation(obstacle.getX(), obstacle.getY(), obstacle.getNext().getX(), obstacle.getNext().getY());
			if(equation_slope[1]!=0){
				slope = Math.abs(equation_slope[0]/equation_slope[1]);
			}
			else
				slope = slope_max;
		}

		if(slope<slope_max && (x+i)<map.getWidth() && (x+i)>0) {
			this.x = x+i;
			this.y = (int) (y+2*i*slope+10);
		}
		
		tankFalls();
	}	
	
	public void tankFalls() {
		Functions f = new Functions();
		int x_t = this.getX();
		int y_t = this.getY();
		
		Coordinate c1 = new Coordinate(x_t, y_t);
		c1.setNext(new Coordinate(x_t, y_t - 5));
		
		Coordinate res = null;
		Polygon Pol = map.getDecor().getFirst(); 
		while(Pol != null && res==null) {
			res = f.isSecantWithPolygon(c1, Pol);
			Pol = Pol.getNext();
		}
		
		if(res == null && y_t > 5){
			this.setY(y_t - 1);
			this.tankFalls();
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	public Weapon getWeapon() {
		return weapon;
	}
}
