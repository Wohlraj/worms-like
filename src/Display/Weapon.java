/**
 * @author COURDURIER Alain
 */
package Display;
public class Weapon {
	
	private int damage;
	private int speed;
	private int damageArea;
	private String name;
	
	public int getDamage(){
		return damage;

	}
	public int getDamageArea(){
		return damageArea;

	}
		/**
		 * @param name
		 * @param damageArea
		 * @param damage
		 */
	public Weapon(String name, int damageArea, int damage, int speed) {
		this.damageArea = damageArea;
		this.damage = damage;
		this.speed = speed; 
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}
	
	public String toString(){
		return name;
	}
}
